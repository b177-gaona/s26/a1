// Create an exercise folder and inside create an ex1.js.

// Import the http module using the required directive.
let http = require("http");

// Create a variable port and assign it with the value of 4001.
const port = 4001;

// Create a server using the createServer method that will listen in to the port provided.
const server = http.createServer((request, response) => {
    // Create a condition that when in the /welcome route is accessed, it will print a message to the user saying "Welcome to the world of Node.js"
    // Access the the welcome route to test if it is working.
    if(request.url == '/welcome') {
        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end(`Welcome to the world of Node.js`);
    }
    // Create another condition that when in the /register route is accessed, it will print a message to the user saying "Page is under maintenance" with status code of 503.
    // Access the register route to test if the route is running properly.
    else if(request.url == '/register'){
        response.writeHead(503, {'Content-Type': 'text/plain'})
        response.end(`Status Code ${response.statusCode}\nPage is under maintenance`);
    }
    // Create a last condition that will return an error message if accessing other routes.
    // Access the last route to test again if it is working.
    else{
        response.writeHead(404, {'Content-Type': 'text/plain'})
        response.end(`Error ${response.statusCode}\nPage not available`);
    }
});

server.listen(port);

// Console log in the terminal a message when the server is successfully running.
console.log('Server running at localhost:4001');

 



