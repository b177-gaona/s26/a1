use('test');
// Use the previous fruits collection
// Create a mongo1.js to store your aggregate solutions
// Create an aggregate that will get the AVERAGE STOCK of fruits on sale.
// Get the AVERAGE STOCK of fruits based on specific SUPPLIERS.
// Push your work to git.

// Average stock of fruits on sale
db.fruits.aggregate([
    {
        $match: {
        onSale:true
    }},
    {
        $group: {
        _id: null,
        averageStock: {
            $avg: "$stock"
        }
    }},
    {
        $project: {
          _id: 0
        }
    }         
]);

// Average stock of fruits on sale per supplier
db.fruits.aggregate([
    {
        $match: {
        onSale:true
    }},
    {
        $group: {
        _id: "$supplier_id",
        averageStockPerSupplier: {
            $avg: "$stock"
        }
    }}   
]);