use('test');
// Still using the previous fruits collection, create an aggregate that will get the MAXIMUM PRICE of fruits on sale.
// Create a mongo2.js to store your aggregate solutions
// Get the MAX PRICE in origin "Philippines".
// Push your work to git.

// Maximum price of fruits on sale
db.fruits.aggregate([
    {
        $match: {
        onSale:true
    }},
    {
        $group: {
        _id: null,
        maxPrice: {
            $max: "$price"
        }
    }},
    {
        $project: {
          _id: 0
        }
    }         
]);

// Max price with origin Philippines
db.fruits.aggregate([
    {
        $match: {
        onSale: true,
        origin: "Philippines"
    }},
    {
        $group: {
        _id: null,
        maxPricePhilippines: {
            $max: "$price"
        }
    }},
    {
        $project: {
          _id: 0
        }
    }         
]);