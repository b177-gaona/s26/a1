// Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,
// Sample output in the console:
// 1 - odd
// 2 - even
// 3 - odd
// 4 - even
// 5 - odd
// etc.

for (let i=1; i<=300; i++) {
    if (i%2 == 0) {
        isEven="even"
    } else {
        isEven="odd"
    };
    console.log(`${i} - ${isEven}`);
}
